import { Map } from 'immutable';
import { gnarReducers } from 'gnar-edge/es/redux';
import { notifications } from 'gnar-edge/es/notifications';
import { routerReducer as routing } from 'react-router-redux';

import { ENGLISH } from 'i18n';
import { SET_LANGUAGE } from 'actions';

export const ACCOUNT_ACTIVATED = 'ACCOUNT_ACTIVATED';
export const ACCOUNT_ACTIVATED_ERROR = 'ACCOUNT_ACTIVATED_ERROR';
export const ACCOUNT_DETAILS = 'ACCOUNT_DETAILS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const SET_PASSWORD_ERROR = 'SET_PASSWORD_ERROR';
export const SET_PASSWORD_SCORE = 'SET_PASSWORD_SCORE';
export const SET_PWNED_PASSWORD_COUNT = 'SET_PWNED_PASSWORD_COUNT';

const checkDispatchedAt = (state, { payload }) =>
  (payload.passwordScoreDispatchedAt > state.get('passwordScoreDispatchedAt', 0) ? state.merge(payload) : state);

export default gnarReducers({
  accountDetails: {
    customReducers: {
      [ACCOUNT_DETAILS]: (state, { payload }) => state.clear().merge(payload)
    }
  },
  activate: {
    basicReducers: [ ACCOUNT_ACTIVATED, ACCOUNT_ACTIVATED_ERROR ]
  },
  language: {
    initialState: Map({ current: ENGLISH }),
    basicReducers: [ SET_LANGUAGE ]
  },
  login: {
    basicReducers: [ LOGIN_ERROR ]
  },
  notifications,
  passwordScore: {
    customReducers: {
      [SET_PASSWORD_SCORE]: checkDispatchedAt
    }
  },
  pwnedPasswordCount: {
    customReducers: {
      [SET_PWNED_PASSWORD_COUNT]: checkDispatchedAt
    }
  },
  routing,
  setPassword: {
    basicReducers: [ SET_PASSWORD_ERROR ]
  }
}, Map);
