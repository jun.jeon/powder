import { getJwt, isLoggedIn } from 'gnar-edge/es/jwt';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Redirect from 'react-router-dom/Redirect';
import Route from 'react-router-dom/Route';

export default class AuthenticatedRoute extends PureComponent {
  static propTypes = {
    component: PropTypes.func.isRequired
  }

  render() {
    const { component: RouteComponent, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={props => (isLoggedIn()
          ? <RouteComponent email={getJwt().identity} {...props} />
          : <Redirect to='/login' />)}
      />
    );
  }
}
