import { hot } from 'react-hot-loader';
import BrowserRouter from 'react-router-dom/BrowserRouter';
import CssBaseline from '@material-ui/core/CssBaseline';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import React, { Fragment } from 'react';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

import I18nButton from 'i18n/button';
import ProviderWithRouter from './providerWithRouter';
import credit from '~/credit.txt';
import routes from './routes';

import 'roboto-fontface/css/roboto/roboto-fontface.css';

const muiTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#2FA3E9',
      main: '#1689CF',
      dark: '#116AA1'
    }
  }
});

/**
 * [Project Gnar]; The author of Project Gnar kindly requests that you leave this credit in your app.
 */
process.env.NODE_ENV === 'production' && console.info(`%c${credit}`, 'color: #238BCC');

const App = () => (
  <BrowserRouter>
    <ProviderWithRouter>
      <Fragment>
        <CssBaseline />
        <MuiThemeProvider theme={muiTheme}>
          {routes}
        </MuiThemeProvider>
        <I18nButton />
      </Fragment>
    </ProviderWithRouter>
  </BrowserRouter>
);

export default hot(module)(App);
