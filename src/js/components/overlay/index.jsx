import PropTypes from 'prop-types';
import React, { Component } from 'react';
import blueGrey from '@material-ui/core/colors/blueGrey';
import classnames from 'classnames';
import withStyles from '@material-ui/core/styles/withStyles';

import './index.scss';

const styles = () => ({
  overlay: {
    backgroundImage:
      `linear-gradient(135deg,
        ${blueGrey['200']} 25%,
        transparent 25%,
        transparent 50%,
        ${blueGrey['200']} 50%,
        ${blueGrey['200']} 75%,
        transparent 75%,
        transparent
      )`
  }
});

@withStyles(styles)
export default class Overlay extends Component {
  static propTypes = {
    classes: PropTypes.shape({
      overlay: PropTypes.string.isRequired
    }).isRequired
  };

  render() {
    const { classes } = this.props;
    return <div className={classnames(classes.overlay, 'overlay')} />;
  }
}
